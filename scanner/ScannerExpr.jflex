package ubordeaux.deptinfo.compilation.project.main;

import beaver.Symbol;
import beaver.Scanner;

%%

%class ScannerExpr
%extends Scanner
%function nextToken
%type Symbol
%yylexthrow Scanner.Exception
%eofval{
	System.out.println(yytext()); 
	return new Symbol(Terminals.EOF);
%eofval}
%unicode
%line
%column

Identifier = [a-zA-Z_][a-zA-Z_]*
Integer = [0-9]+
Hexadecimal =  [0x]+[0-9]*[A-Z]*[0-9A-Z_]*

%state C_COMMENT
%state CPLUSPLUS_COMMENT
%state STRING

%%

<YYINITIAL>{

"//"			{ yybegin(C_COMMENT);}
"/*"			{ yybegin(CPLUSPLUS_COMMENT);}
"\""			{ yybegin(STRING);}

".."			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_DOTDOT, yyline, yycolumn);}
"boolean"		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_BOOLEAN, yyline, yycolumn);}
"string"		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_STRING, yyline, yycolumn);}
"array"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_ARRAY, yyline, yycolumn);}
"of"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_OF, yyline, yycolumn);}
"struct"		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_STRUCT, yyline, yycolumn);}

"println"		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_PRINTLN, yyline, yycolumn);}
"return"		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_RETURN, yyline, yycolumn);}

"null"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_NULL, yyline, yycolumn);}
"true"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_TRUE, yyline, yycolumn);}
"false"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_FALSE, yyline, yycolumn);}

"if"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_IF, yyline, yycolumn);}
"then"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_THEN, yyline, yycolumn);}
"else"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_ELSE, yyline, yycolumn);}
"while"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_WHILE, yyline, yycolumn);}
"do"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_DO, yyline, yycolumn);}

"begin"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_BEGIN, yyline, yycolumn);}
"end" 			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_END, yyline, yycolumn);}
"var"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_VAR, yyline, yycolumn);}

"+" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_PLUS, yyline, yycolumn); }
"-" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_MINUS, yyline, yycolumn); }
"*" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_TIMES, yyline, yycolumn); }
"/" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_DIV, yyline, yycolumn); }
"(" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LPAR, yyline, yycolumn); }
")" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_RPAR, yyline, yycolumn); }
"=" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_AFF, yyline, yycolumn); }

"!"				{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_NOT, yyline, yycolumn); }
"&&"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_AND, yyline, yycolumn); }
"||"			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_OR, yyline, yycolumn); }

":"				{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_COLON, yyline, yycolumn); }
"," 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_COMMA, yyline, yycolumn); }
";" 	        { System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_SEMIC, yyline, yycolumn); }
"["				{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LBRACKET, yyline, yycolumn); }
"]" 			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_RBRACKET, yyline, yycolumn); }

"<"				{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LT, yyline, yycolumn);}
"<="			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LE, yyline, yycolumn);}
">"				{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_GT, yyline, yycolumn);}
">="			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_GE, yyline, yycolumn);}
"=="			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_EQ, yyline, yycolumn);}
"!="			{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_NE, yyline, yycolumn);}

{Identifier}	{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_IDENTIFIER, yyline, yycolumn, new String(yytext()));}
{Integer}		{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LIT_INTEGER, yyline, yycolumn, new Integer(yytext())); }
{Hexadecimal}	{ System.out.println("*** " + yytext()); return new Symbol(Terminals.TOKEN_LIT_INTEGER, yyline, yycolumn, new Integer(yytext())); }

[^]|\n      	{ }

}

<STRING>{

"\""				{ yybegin(YYINITIAL); return new Symbol(Terminals.TOKEN_LIT_STRING, yyline, yycolumn);}
{Identifier}		{ System.out.println("*** " + yytext());}
[^]					{}

}

<CPLUSPLUS_COMMENT>{

"*/"				{ yybegin(YYINITIAL); return new Symbol(Terminals.TOKEN_LIT_STRING, yyline, yycolumn); }
{Identifier}		{  System.out.println("*** " + yytext());}
[^]					{}

}

<C_COMMENT>{

\n					{ yybegin(YYINITIAL); return new Symbol(Terminals.TOKEN_LIT_STRING, yyline, yycolumn);}
{Identifier}		{ System.out.println("*** " + yytext()); }
[^] 				{}

}