package ubordeaux.deptinfo.compilation.project.main;

/**
 * This class lists terminals used by the
 * grammar specified in "ParserExpr.grammar".
 */
public class Terminals {
	static public final short EOF = 0;
	static public final short TOKEN_IDENTIFIER = 1;
	static public final short TOKEN_LPAR = 2;
	static public final short TOKEN_LIT_INTEGER = 3;
	static public final short TOKEN_UNARY_MINUS = 4;
	static public final short TOKEN_NOT = 5;
	static public final short TOKEN_LIT_STRING = 6;
	static public final short TOKEN_TRUE = 7;
	static public final short TOKEN_FALSE = 8;
	static public final short TOKEN_NULL = 9;
	static public final short TOKEN_CIRC = 10;
	static public final short TOKEN_LT = 11;
	static public final short TOKEN_LE = 12;
	static public final short TOKEN_GT = 13;
	static public final short TOKEN_GE = 14;
	static public final short TOKEN_EQ = 15;
	static public final short TOKEN_NE = 16;
	static public final short TOKEN_BEGIN = 17;
	static public final short TOKEN_PLUS = 18;
	static public final short TOKEN_MINUS = 19;
	static public final short TOKEN_TIMES = 20;
	static public final short TOKEN_DIV = 21;
	static public final short TOKEN_SEMIC = 22;
	static public final short TOKEN_OR = 23;
	static public final short TOKEN_AND = 24;
	static public final short TOKEN_VAR = 25;
	static public final short TOKEN_RPAR = 26;
	static public final short TOKEN_IF = 27;
	static public final short TOKEN_SWITCH = 28;
	static public final short TOKEN_NEW = 29;
	static public final short TOKEN_DISPOSE = 30;
	static public final short TOKEN_PRINTLN = 31;
	static public final short TOKEN_READLN = 32;
	static public final short TOKEN_RETURN = 33;
	static public final short TOKEN_WHILE = 34;
	static public final short TOKEN_ARRAY = 35;
	static public final short TOKEN_STRUCT = 36;
	static public final short TOKEN_STRING = 37;
	static public final short TOKEN_INTEGER = 38;
	static public final short TOKEN_BOOLEAN = 39;
	static public final short TOKEN_COLON = 40;
	static public final short TOKEN_LBRACKET = 41;
	static public final short TOKEN_RBRACKET = 42;
	static public final short TOKEN_COMMA = 43;
	static public final short TOKEN_END = 44;
	static public final short TOKEN_AFF = 45;
	static public final short TOKEN_FUNCTION = 46;
	static public final short TOKEN_PROCEDURE = 47;
	static public final short TOKEN_DOTDOT = 48;
	static public final short TOKEN_CASE = 49;
	static public final short TOKEN_TYPE = 50;
	static public final short TOKEN_OF = 51;
	static public final short TOKEN_LBRACE = 52;
	static public final short TOKEN_RBRACE = 53;
	static public final short TOKEN_THEN = 54;
	static public final short TOKEN_ELSE = 55;
	static public final short TOKEN_DO = 56;
	static public final short TOKEN_DEFAULT = 57;

	static public final String[] NAMES = {
		"EOF",
		"TOKEN_IDENTIFIER",
		"TOKEN_LPAR",
		"TOKEN_LIT_INTEGER",
		"TOKEN_UNARY_MINUS",
		"TOKEN_NOT",
		"TOKEN_LIT_STRING",
		"TOKEN_TRUE",
		"TOKEN_FALSE",
		"TOKEN_NULL",
		"TOKEN_CIRC",
		"TOKEN_LT",
		"TOKEN_LE",
		"TOKEN_GT",
		"TOKEN_GE",
		"TOKEN_EQ",
		"TOKEN_NE",
		"TOKEN_BEGIN",
		"TOKEN_PLUS",
		"TOKEN_MINUS",
		"TOKEN_TIMES",
		"TOKEN_DIV",
		"TOKEN_SEMIC",
		"TOKEN_OR",
		"TOKEN_AND",
		"TOKEN_VAR",
		"TOKEN_RPAR",
		"TOKEN_IF",
		"TOKEN_SWITCH",
		"TOKEN_NEW",
		"TOKEN_DISPOSE",
		"TOKEN_PRINTLN",
		"TOKEN_READLN",
		"TOKEN_RETURN",
		"TOKEN_WHILE",
		"TOKEN_ARRAY",
		"TOKEN_STRUCT",
		"TOKEN_STRING",
		"TOKEN_INTEGER",
		"TOKEN_BOOLEAN",
		"TOKEN_COLON",
		"TOKEN_LBRACKET",
		"TOKEN_RBRACKET",
		"TOKEN_COMMA",
		"TOKEN_END",
		"TOKEN_AFF",
		"TOKEN_FUNCTION",
		"TOKEN_PROCEDURE",
		"TOKEN_DOTDOT",
		"TOKEN_CASE",
		"TOKEN_TYPE",
		"TOKEN_OF",
		"TOKEN_LBRACE",
		"TOKEN_RBRACE",
		"TOKEN_THEN",
		"TOKEN_ELSE",
		"TOKEN_DO",
		"TOKEN_DEFAULT",
	};
}
