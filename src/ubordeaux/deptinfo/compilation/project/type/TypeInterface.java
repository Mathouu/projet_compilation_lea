package ubordeaux.deptinfo.compilation.project.type;

public interface TypeInterface {

	// Atteste que le type est bien formé
	public boolean attestWellFormed();
	
	// Affiche le type dans un format lisible
	public String toString();

	// teste l'egalite entre deux types
	public boolean equals(Type obj);

	// Realise l'unification d'un type avec un autre.
	// La valeur de retour est fausse si l'unification echoue
	// et dans le cas contraire, le resultat se trouve dans result.
	// Cette methode n'est pas utilisee dans le projet, mais peut l'etre 
	// par la suite
	public boolean unifyWith(Subst result, Type obj);

}
